package Network;

import java.io.*;
import java.net.*;

import java.util.Properties;
import java.util.Scanner;

public class Server {
    public static void main(String[]args) {
        String port = "";
        Scanner input = new Scanner(System.in);
        try {
            try (InputStream add = new FileInputStream("/Users/ada-nb182/Downloads/credentials.txt")) {

                Properties prop = new Properties();

                prop.load(add);

                port = prop.getProperty("port");

            } catch (IOException ex) {
                ex.printStackTrace();
            }
            ServerSocket serverSocket = new ServerSocket(6666);
            Socket socket = serverSocket.accept();

            DataInputStream dis=new DataInputStream(socket.getInputStream());
            DataOutputStream dout=new DataOutputStream(socket.getOutputStream());

            String konteks = "";

            while (!konteks.equalsIgnoreCase("exit")) {
                System.out.println("Menunggu client...");
                konteks=(String)dis.readUTF();
                System.out.println("Client= "+konteks);

                if(konteks.equalsIgnoreCase("exit")){
                    System.out.println("Client Keluar");
                    break;
                }

                System.out.println("Client: ");
                konteks = input.nextLine();
                dout.writeUTF(konteks);
                dout.flush();
            }
            serverSocket.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }

}
