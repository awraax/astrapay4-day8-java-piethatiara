package Network;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Scanner;

public class Client {
    public static void main(String[]args) {
        String ip = "", port = "";
        Scanner input = new Scanner(System.in);

        try {
            try (InputStream add = new FileInputStream("/Users/ada-nb182/Downloads/credentials.txt")) {

                Properties prop = new Properties();

                prop.load(add);

                ip = prop.getProperty("ip");
                port = prop.getProperty("port");

            } catch (IOException ex) {
                ex.printStackTrace();
            }

            Socket socket=new Socket(ip, Integer.parseInt(port));

            DataOutputStream dout=new DataOutputStream(socket.getOutputStream());
            DataInputStream dis=new DataInputStream(socket.getInputStream());

            String konteks = "";

            while (!konteks.equalsIgnoreCase("exit")){

                System.out.println("Kirim pesan ke server:");
                konteks = input.nextLine();
                dout.writeUTF(konteks);
                dout.flush();


                if(konteks.equalsIgnoreCase("exit")){
                    System.out.println("Server exited");
                    break;
                } else{
                    System.out.println("Menunggu server....");
                    konteks = (String)dis.readUTF();
                    System.out.println("Server said= "+konteks);
                }

            }
            dout.close();
            socket.close();
        }catch (Exception e){
            System.out.println(e);
        }

    }
}
