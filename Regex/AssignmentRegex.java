package Regex;

import java.util.Scanner;

public class AssignmentRegex {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //Regex email dan password bisa di cari pattern nya.
        String regexEmail = "^[A-Za-z0-9+_.-]+@(.+)$";
        String regexPassword ="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{8,}$";

        System.out.println("Email : ");
        String email = input.next();
        System.out.println("Password : ");
        String password = input.next();

        System.out.println(email + " : " +email.matches(regexEmail));
        System.out.println(password + " : " +password.matches(regexPassword));
    }

}
