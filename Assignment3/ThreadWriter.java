package Assignment3;

import java.io.FileWriter;
import java.util.ArrayList;

public class ThreadWriter extends Thread{
    private ArrayList<Mahasiswa> arrayMahasiswa = new ArrayList<Mahasiswa>();

    public ThreadWriter(ArrayList<Mahasiswa> arrayMahasiswa) {
        this.arrayMahasiswa = arrayMahasiswa;}

    public void run () {

        try {
            FileWriter fw = new FileWriter("/Users/ada-nb178/Documents/Assignment G2/Da8Java/src/Task3/Laporan.txt");
            for (Mahasiswa s : arrayMahasiswa) {
                fw.write("ID: " + s.getIdMahasiswa());
                fw.write("\n");
                fw.write("Nama: " + s.getNamaMahasiswa());
                fw.write("\n");
                fw.write("Nilai Bhs Inggris: " + s.getBahasaInggris());
                fw.write("\n");
                fw.write("Nilai Fisika: " + s.getFisika());
                fw.write("\n");
                fw.write("Nilai Algoritma: " + s.getAlgoritma());

                fw.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("File berhasil dibuat...");
    }

}
