package Assignment3;

class Mahasiswa extends NilaiMahasiswa{
    int idMahasiswa;
    String namaMahasiswa;


    public Mahasiswa (int idMahasiswa, String namaMahasiswa, double bahasaInggris, double fisika, double algoritma){
        super(bahasaInggris, fisika, algoritma);
        this.idMahasiswa = idMahasiswa;
        this.namaMahasiswa = namaMahasiswa;
    }


    public int getIdMahasiswa() {
        return idMahasiswa;
    }

    public String getNamaMahasiswa() {
        return namaMahasiswa;
    }

}


