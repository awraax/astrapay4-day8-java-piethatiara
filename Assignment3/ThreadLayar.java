package Assignment3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ThreadLayar extends Thread{
    private ArrayList<Mahasiswa> arrayMahasiswa = new ArrayList<Mahasiswa>();
    public ThreadLayar(ArrayList<Mahasiswa> arrayMahasiswa){
        this.arrayMahasiswa = arrayMahasiswa;
    }
    public void run(){
        Collections.sort(arrayMahasiswa, Comparator.comparingLong(Mahasiswa::getIdMahasiswa));
        String leftAlignFormat = "| %-4d | %-15s | %-15s | %-15s | %-15s |%n";
        System.out.format("+------+-----------------+-----------------+-----------------+-----------------+%n");
        System.out.format("| ID   | Nama            | Bhs Inggris     | Fisika          | Algoritma       |%n");
        System.out.format("+------+-----------------+-----------------+-----------------+-----------------+%n");
        Collections.sort(arrayMahasiswa, Comparator.comparingLong(Mahasiswa::getIdMahasiswa));
        for (Mahasiswa s : arrayMahasiswa) {
            System.out.format(leftAlignFormat, s.getIdMahasiswa(), s.getNamaMahasiswa(), s.getBahasaInggris(),s.getFisika(), s.getAlgoritma());
        }
        System.out.format("+------+-----------------+-----------------+-----------------+-----------------+%n");
    }

}
