package Assignment3;


class NilaiMahasiswa {
    Double bahasaInggris;
    Double fisika;
    Double algoritma;

    public NilaiMahasiswa (Double bahasaInggris, Double fisika, Double algoritma){
        this.bahasaInggris = bahasaInggris;
        this.fisika = fisika;
        this.algoritma = algoritma;
    }

    public Double getBahasaInggris() {
        return bahasaInggris;
    }

    public Double getFisika() {
        return fisika;
    }

    public Double getAlgoritma() {
        return algoritma;
    }
}
