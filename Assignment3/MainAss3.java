package Assignment3;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Pattern;

public class MainAss3 {
    public static void main(String[] args) {
        do {
            String cekEmail = "";
            String cekPass = "";
            Scanner input = new Scanner(System.in);
            System.out.print("Masukkan Email = ");
            String email = input.next();
            System.out.print("Masukkan Password = ");
            String pass = input.next();

            boolean mail = Pattern.matches("^[A-Za-z0-9+_.-]+@(.+)$", email);
            boolean passw = Pattern.matches("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{8,}$", pass);
            System.out.println("Username = " + mail + "\nPassword = " +passw);

            if (mail == true){
                try (InputStream add = new FileInputStream("/Users/ada-nb182/Downloads/credentials.txt")){
                    Properties properties = new Properties();
                    properties.load(add);
                    cekEmail = properties.getProperty("username");
                    cekPass = properties.getProperty("password");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ArrayList<Mahasiswa> arrayMahasiswa = new ArrayList<>();
                int pilih = 0;
                if (email.equalsIgnoreCase(cekEmail) && (pass.equalsIgnoreCase(cekPass))){
                    System.out.println("Anda Berhasil Login");
                    do {
                            System.out.println("\nMENU");
                            System.out.println("1. Create & Input Data Mahasiswa");
                            System.out.println("2. Tampilkan Laporan Nilai Data Mahasiswa");
                            System.out.println("3. Tampilkan di Layar & Tulis ke File");
                            System.out.println("4. Exit");

                            System.out.print("Pilih Menu = ");
                            pilih = input.nextInt();

                            switch (pilih){
                                case 1:
                                    System.out.print("Input ID Mahasiswa = ");
                                    int idMaha = input.nextInt();
                                    System.out.print("Input Nama Mahasiswa = ");
                                    String namaMaha = input.next();
                                    System.out.println("Input Nilai Mahasiswa : ");

                                    System.out.print("Bahasa Inggris = ");
                                    Double inggris = input.nextDouble();
                                    System.out.print("Fisika = ");
                                    Double fisika = input.nextDouble();
                                    System.out.print("Algoritma = ");
                                    Double algoritma = input.nextDouble();

                                    Mahasiswa mahasiswa = new Mahasiswa(idMaha,namaMaha,inggris,fisika,algoritma);
                                    arrayMahasiswa.add(mahasiswa);
                                    break;
                                case 2:
                                    Collections.sort(arrayMahasiswa, Comparator.comparingLong(Mahasiswa::getIdMahasiswa));
                                    String leftAlignFormat = "| %-4d | %-15s | %-15s | %-15s | %-15s |%n";
                                    System.out.format("+------+-----------------+-----------------+%n");
                                    System.out.format("| ID   | Nama            | Bahasa Inggris  | Fisika          | Algoritma       |%n");
                                    System.out.format("+------+-----------------+-----------------+%n");
                                    for (Mahasiswa m : arrayMahasiswa) {
                                        System.out.format(leftAlignFormat, m.getIdMahasiswa(), m.getNamaMahasiswa(), m.getBahasaInggris(), m.getFisika(), m.getAlgoritma());
                                    }
                                    System.out.format("+------+-----------------+-----------------+%n");
                                    break;
                                case 3:
                                    ThreadLayar t1=new ThreadLayar(arrayMahasiswa);
                                    t1.start();

                                    //Write data ke file
                                    ThreadWriter t2=new ThreadWriter(arrayMahasiswa);
                                    t2.start();
                                    break;
                                default:
                                    System.out.println("Pilihan tidak tersedia");
                                    break;
                            }
                    }while (pilih != 4);
                } else {
                    System.out.println("Email dan Password Belum Sesuai");
                }
            }else {
                System.out.println("Masukkan Email dan Password yang Sesuai");
            }

        }while (true);

    }
}
